angular.module('alurapic').controller('FotosController', function($scope, recursoFoto){

   $scope.fotos = [];
   $scope.filtro = '';
   $scope.mensagem = '';

   recursoFoto.query(function(fotos){
     $scope.fotos = fotos;
   },function(erro){
     console.error(erro);
   })
/*
   $http.get('http://localhost:3000/v1/fotos')
   .success(function(fotos){
     $scope.fotos = fotos;
   }).error(function(erro){
     console.error('As fotos nao foram carregadas');
   });

   $scope.remover = function(foto){
      $http.delete('/v1/fotos/'+foto._id)
       .success(function(){
         $scope.fotos.splice($scope.fotos.indexOf(foto),1);
         $scope.mensagem = "foto " + foto.titulo + "removida com sucesso";
       })
       .error(function(){
          console.log('error');
         $scope.mensagem = "nao foi possivel remover a foto";
       });
   }
  */

  $scope.remover = function(foto){
    recursoFoto.delete({fotoId: foto._id}, function(){
      var indiceFoto = $scope.fotos.indexOf(foto);
      $scope.fotos.splice(indiceFoto, 1);
      $scope.mensagem = "foto " + foto.titulo + "removida com sucesso";
    },
      function(erro){
        $scope.mensagem = "nao foi possivel remover a foto";
      }
    )
  }
})

angular.module('alurapic').filter('fotosFilter', function(){
  return function(lista, filtro){
    //console.log(lista);
    //console.log(filtro);

    if(filtro == undefined || filtro == "")
      return lista;

    if(lista != undefined)
      return lista.filter(function(foto){ foto.titulo.match(new RegExp("\\b"+filtro)) })
  }
})
