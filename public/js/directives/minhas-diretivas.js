angular.module('minhasDiretivas',[]).directive('meuPainel', function(){
  var ddo = {}
  ddo.restrict = 'AE'
  ddo.scope = {
    titulo : '@titulo'
  }

  ddo.templateUrl = 'js/directives/meu-painel.html'
  ddo.transclude = true
  return ddo
})

angular.module('minhasDiretivas').directive('minhaFoto', function(){
  var ddo = {}
  ddo.restrict = 'AE'
  ddo.scope = {
    url : '@url'
    ,titulo2 : '@titulo2'
  }
  ddo.templateUrl = 'js/directives/minha-foto.html'
  return ddo
})
